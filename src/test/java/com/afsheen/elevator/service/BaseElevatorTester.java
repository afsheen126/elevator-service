package com.afsheen.elevator.service;

import java.util.List;

import com.afsheen.elevator.domain.ElevatorOperatingMode;

public abstract class BaseElevatorTester {

	protected ElevatorTripPlan retrieveElevatorTripPlan(ElevatorOperatingMode mode, List<Integer> legs) {
		ElevatorService elevService = (ElevatorOperatingMode.A == mode) ? new ElevatorInModeAServiceImpl()
				: new ElevatorInModeBServiceImpl();

		ElevatorTripPlan plan = elevService.getElevatorTripPlan(legs);

		return plan;
	}

}
