package com.afsheen.elevator.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.afsheen.elevator.domain.ElevatorOperatingMode;

public class ElevatorServiceInvokerTester {

	//
	//
	//
	//
	// TODO : update BASE_PATH variable to match the location of the test_input
	// files in your workspace
	//
	//
	//
	private static final String BASE_PATH = "//Users//Afsheen//Documents//workspace//elevator-service//src//test//resources//";

	private static final String INPUT_FILE_NAME_00 = BASE_PATH + "test_input_00.txt";
	private static final String INPUT_FILE_NAME_01 = BASE_PATH + "test_input_01.txt";
	private static final String INPUT_FILE_NAME_02 = BASE_PATH + "test_input_02.txt";
	private static final String INPUT_FILE_NAME_03 = BASE_PATH + "test_input_03.txt";
	private static final String INPUT_FILE_NAME_04 = BASE_PATH + "test_input_04.txt";
	private static final String INPUT_FILE_NAME_05 = BASE_PATH + "test_input_05.txt";
	private static final String INPUT_FILE_NAME_06 = BASE_PATH + "test_input_06.txt";
	private static final String INPUT_FILE_NAME_07 = BASE_PATH + "test_input_07.txt";
	private static final String INPUT_FILE_NAME_08 = BASE_PATH + "test_input_08.txt";
	private static final String INPUT_FILE_NAME_09 = BASE_PATH + "test_input_09.txt";

	@Test
	public void testThatOperatingModeACanBeSet() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", "input.txt");
		Assert.assertNotNull(invoker);
		Assert.assertEquals(ElevatorOperatingMode.A, invoker.getMode());

		Assert.assertTrue(invoker.getElevatorService() instanceof ElevatorInModeAServiceImpl);
	}

	@Test
	public void testThatOperatingModeBCanBeSet() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("B", "input.txt");
		Assert.assertNotNull(invoker);
		Assert.assertEquals(ElevatorOperatingMode.B, invoker.getMode());

		Assert.assertTrue(invoker.getElevatorService() instanceof ElevatorInModeBServiceImpl);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatOperatingModeCannotBeNull() throws FileNotFoundException, IOException {

		new ElevatorServiceInvoker(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatOperatingModeCanOnlyBeOneCharacter() throws FileNotFoundException, IOException {

		new ElevatorServiceInvoker("CBX", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatOperatingModeCanOnlyBeAlphabetic() throws FileNotFoundException, IOException {

		new ElevatorServiceInvoker("3", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatOperatingModeCanOnlyBeUppercase() throws FileNotFoundException, IOException {

		new ElevatorServiceInvoker("a", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatTryingToSetNullFileNameThrowsFileNotFoundException() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", null);
		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatTryingToSetEmptyFileNameThrowsFileNotFoundException()
			throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", "");
		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatTryingToSetBlankFileNameThrowsFileNotFoundException()
			throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", "  ");
		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatEmptyFileIsNotProcessed() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_00);
		Assert.assertNotNull(invoker);

		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatFileIsNotProcessedIfAnyLineIsInvalid01() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_01);
		Assert.assertNotNull(invoker);

		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatFileIsNotProcessedIfAnyLineIsInvalid02() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_02);
		Assert.assertNotNull(invoker);

		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatFileIsNotProcessedIfAnyLineIsInvalid03() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_03);
		Assert.assertNotNull(invoker);

		invoker.execute();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testThatFileIsNotProcessedIfAnyLineIsInvalid04() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_04);
		Assert.assertNotNull(invoker);

		invoker.execute();
	}

	@Test
	public void testThatFileIsProcessedForFileWithOneLineHavingOneTrip() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_05);
		Assert.assertNotNull(invoker);

		List<ElevatorTripPlan> plans = invoker.execute();
		Assert.assertNotNull(plans);
		Assert.assertEquals(1, plans.size());

		ElevatorTripPlan plan = plans.get(0);
		Assert.assertNotNull(plan);
		Assert.assertEquals("8 1 (7)", plan.getTripPlan());
	}

	@Test
	public void testThatFileIsProcessedForFileWithOneLineHavingManyTrips() throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("B", INPUT_FILE_NAME_06);
		Assert.assertNotNull(invoker);

		List<ElevatorTripPlan> plans = invoker.execute();
		Assert.assertNotNull(plans);
		Assert.assertEquals(1, plans.size());

		ElevatorTripPlan plan = plans.get(0);
		Assert.assertNotNull(plan);
		Assert.assertEquals("10 9 8 1 (9)", plan.getTripPlan());
	}

	@Test
	public void testThatFileIsProcessedForFileWithManyLinesEachHavingOneTrip()
			throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_07);
		Assert.assertNotNull(invoker);

		List<ElevatorTripPlan> plans = invoker.execute();
		Assert.assertNotNull(plans);
		Assert.assertEquals(5, plans.size());

		Assert.assertEquals("8 1 (7)", plans.get(0).getTripPlan());
		Assert.assertEquals("10 9 5 (5)", plans.get(1).getTripPlan());
		Assert.assertEquals("7 6 3 (4)", plans.get(2).getTripPlan());
		Assert.assertEquals("5 6 (1)", plans.get(3).getTripPlan());
		Assert.assertEquals("3 2 3 (2)", plans.get(4).getTripPlan());
	}

	@Test
	public void testThatFileIsProcessedForFileWithManyLinesEachHavingOneOrManyTrips()
			throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("B", INPUT_FILE_NAME_08);
		Assert.assertNotNull(invoker);

		List<ElevatorTripPlan> plans = invoker.execute();
		Assert.assertNotNull(plans);
		Assert.assertEquals(6, plans.size());

		Assert.assertEquals("10 8 1 (9)", plans.get(0).getTripPlan());
		Assert.assertEquals("9 1 5 6 (13)", plans.get(1).getTripPlan());
		Assert.assertEquals("2 4 2 1 6 8 (12)", plans.get(2).getTripPlan());
		Assert.assertEquals("3 5 7 8 9 11 1 (18)", plans.get(3).getTripPlan());
		Assert.assertEquals("7 11 10 6 5 6 8 12 7 4 8 9 (30)", plans.get(4).getTripPlan());
		Assert.assertEquals("6 1 6 8 (12)", plans.get(5).getTripPlan());
	}

	@Test
	public void testThatFileIsProcessedForFileWithManyLinesEachHavingManyTrips()
			throws FileNotFoundException, IOException {

		ElevatorServiceInvoker invoker = new ElevatorServiceInvoker("A", INPUT_FILE_NAME_09);
		Assert.assertNotNull(invoker);

		List<ElevatorTripPlan> plans = invoker.execute();
		Assert.assertNotNull(plans);
		Assert.assertEquals(6, plans.size());

		Assert.assertEquals("10 8 1 2 3 4 9 (17)", plans.get(0).getTripPlan());
		Assert.assertEquals("9 1 5 1 6 1 5 (30)", plans.get(1).getTripPlan());
		Assert.assertEquals("2 4 1 4 2 6 8 (16)", plans.get(2).getTripPlan());
		Assert.assertEquals("3 7 9 3 7 5 8 7 11 1 (36)", plans.get(3).getTripPlan());
		Assert.assertEquals("7 11 6 10 5 6 8 7 4 12 7 8 9 (40)", plans.get(4).getTripPlan());
		Assert.assertEquals("6 1 8 6 8 7 5 2 8 (28)", plans.get(5).getTripPlan());
	}

}
