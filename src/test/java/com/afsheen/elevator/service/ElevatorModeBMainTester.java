package com.afsheen.elevator.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.afsheen.elevator.domain.ElevatorOperatingMode;
import com.afsheen.elevator.domain.Leg;

public class ElevatorModeBMainTester extends BaseElevatorTester {

	protected ElevatorTripPlan retrieveElevatorModeBTripPlan(List<Integer> legs) {
		return retrieveElevatorTripPlan(ElevatorOperatingMode.B, legs);
	}

	@Test
	public void test1() {

		List<Integer> legs = Arrays.asList(10, 8, 8, 1);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(10, 8), new Leg(8, 1));
		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "10 8 1 (9)";
		Assert.assertEquals(expFloorsString, floorsString);
	}

	@Test
	public void test2() {

		List<Integer> legs = Arrays.asList(9, 1, 1, 5, 1, 6, 1, 5);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(9, 1), new Leg(1, 5), new Leg(1, 5), new Leg(1, 6));
		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "9 1 5 6 (13)";
		Assert.assertEquals(expFloorsString, floorsString);

	}

	@Test
	public void test3() {

		List<Integer> legs = Arrays.asList(2, 4, 4, 1, 4, 2, 6, 8);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);
		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(2, 4), new Leg(4, 2), new Leg(4, 1), new Leg(6, 8));

		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "2 4 2 1 6 8 (12)";
		Assert.assertEquals(expFloorsString, floorsString);

	}

	@Test
	public void test4() {

		List<Integer> legs = Arrays.asList(3, 7, 7, 9, 3, 7, 5, 8, 7, 11, 11, 1);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);
		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(3, 7), new Leg(3, 7), new Leg(5, 8), new Leg(7, 9), new Leg(7, 11),
				new Leg(11, 1));

		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "3 5 7 8 9 11 1 (18)";
		Assert.assertEquals(expFloorsString, floorsString);

	}

	@Test
	public void test5() {

		List<Integer> legs = Arrays.asList(7, 11, 11, 6, 10, 5, 6, 8, 7, 4, 12, 7, 8, 9);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(7, 11), new Leg(11, 6), new Leg(10, 5), new Leg(6, 8),
				new Leg(12, 7), new Leg(7, 4), new Leg(8, 9));

		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "7 11 10 6 5 6 8 12 7 4 8 9 (30)";
		Assert.assertEquals(expFloorsString, floorsString);

	}

	@Test
	public void test6() {

		List<Integer> legs = Arrays.asList(6, 1, 1, 8, 6, 8);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(6, 1), new Leg(1, 8), new Leg(6, 8));

		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "6 1 6 8 (12)";
		Assert.assertEquals(expFloorsString, floorsString);
	}

}
