package com.afsheen.elevator.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.afsheen.elevator.domain.ElevatorOperatingMode;
import com.afsheen.elevator.domain.Leg;

public class ElevatorModeBXtraTester extends BaseElevatorTester {

	protected ElevatorTripPlan retrieveElevatorModeBTripPlan(List<Integer> legs) {
		return retrieveElevatorTripPlan(ElevatorOperatingMode.B, legs);
	}

	@Test
	public void test11() {

		List<Integer> legs = Arrays.asList(1, 6, 6, 1, 2, 8, 4, 8, 7, 4, 9, 4);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(1, 6), new Leg(6, 1), new Leg(2, 8), new Leg(4, 8), new Leg(9, 4),
				new Leg(7, 4));
		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "1 6 1 2 4 8 9 7 4 (23)";
		Assert.assertEquals(expFloorsString, floorsString);
	}

	@Test
	public void test12() {

		List<Integer> legs = Arrays.asList(3, 3, 3, 1, 1, 6, 4, 5);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(3, 3), new Leg(3, 1), new Leg(1, 6), new Leg(4, 5));
		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "3 1 4 5 6 (7)";
		Assert.assertEquals(expFloorsString, floorsString);
	}

	@Test
	public void test13() {

		List<Integer> legs = Arrays.asList(1, 1, 1, 12, 2, 11, 3, 10, 4, 9, 5, 8, 6, 7);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(1, 1), new Leg(1, 12), new Leg(2, 11), new Leg(3, 10), new Leg(4, 9),
				new Leg(5, 8), new Leg(6, 7));
		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "1 2 3 4 5 6 7 8 9 10 11 12 (11)";
		Assert.assertEquals(expFloorsString, floorsString);
	}

	@Test
	public void test14() {

		List<Integer> legs = Arrays.asList(1, 1, 1, 1);
		ElevatorTripPlan elev = retrieveElevatorModeBTripPlan(legs);

		List<Leg> trips = elev.getTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(1, 1), new Leg(1, 1));
		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());

		String floorsString = elev.getTripPlan();
		String expFloorsString = "1 (0)";
		Assert.assertEquals(expFloorsString, floorsString);
	}

}
