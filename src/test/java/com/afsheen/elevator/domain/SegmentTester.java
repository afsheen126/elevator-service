package com.afsheen.elevator.domain;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.afsheen.elevator.domain.ElevatorInModeB.Segment;

public class SegmentTester {

	@Test(expected = IllegalArgumentException.class)
	public void testSegmentCannotBeCreatedWithNullLeg() {
		new Segment(null);
	}

	@Test
	public void testSegmentCanHaveJustOneLeg() {
		Segment seg = new Segment(new Leg(4, 6));
		Assert.assertNotNull(seg);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSegmentCannotHaveLegsGoingInOppositeDirections() {
		Segment seg = new Segment(new Leg(4, 6));
		Assert.assertNotNull(seg);

		seg.add(new Leg(7, 1));
	}

	@Test
	public void testSegmentCanReturnSortedLegs() {
		Leg[] legs = new Leg[] { new Leg(4, 6), new Leg(1, 8), new Leg(5, 9) };

		Segment seg = new Segment(legs[0]);
		Assert.assertNotNull(seg);

		for (int index = 1; index < legs.length; index++) {
			seg.add(legs[index]);
		}

		List<Leg> trips = seg.getSortedTrips();

		List<Leg> expTrips = Arrays.asList(new Leg(1, 8), new Leg(4, 6), new Leg(5, 9));

		Assert.assertArrayEquals(expTrips.toArray(), trips.toArray());
	}

}
