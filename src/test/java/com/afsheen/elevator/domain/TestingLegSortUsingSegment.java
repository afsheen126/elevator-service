package com.afsheen.elevator.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.afsheen.elevator.domain.ElevatorInModeB.Segment;

public class TestingLegSortUsingSegment {

	@Test
	public void test1() {

		Segment segment = new Segment(new Leg(8, 1));

		List<Leg> expectedLegs = new ArrayList<Leg>();
		expectedLegs.add(new Leg(8, 1));

		List<Leg> legs = segment.getSortedTrips();
		Assert.assertArrayEquals(expectedLegs.toArray(), legs.toArray());
	}

	@Test
	public void test2() {

		Segment seg = new Segment(new Leg(1, 5));
		seg.add(new Leg(1, 6));
		seg.add(new Leg(1, 5));

		List<Leg> expectedLegs = new ArrayList<Leg>();
		expectedLegs.addAll(Arrays.asList(new Leg(1, 5), new Leg(1, 5), new Leg(1, 6)));

		List<Leg> legs = seg.getSortedTrips();
		Assert.assertArrayEquals(expectedLegs.toArray(), legs.toArray());
	}

	@Test
	public void test3() {

		Segment seg = new Segment(new Leg(4, 8));
		seg.add(new Leg(5, 7));
		seg.add(new Leg(4, 6));

		List<Leg> expectedLegs = new ArrayList<Leg>();
		expectedLegs.addAll(Arrays.asList(new Leg(4, 6), new Leg(4, 8), new Leg(5, 7)));

		List<Leg> legs = seg.getSortedTrips();
		Assert.assertArrayEquals(expectedLegs.toArray(), legs.toArray());
	}

	@Test
	public void test4() {

		Segment seg = new Segment(new Leg(4, 7));
		seg.add(new Leg(5, 6));
		seg.add(new Leg(1, 6));

		List<Leg> expectedLegs = new ArrayList<Leg>();
		expectedLegs.addAll(Arrays.asList(new Leg(1, 6), new Leg(4, 7), new Leg(5, 6)));

		List<Leg> legs = seg.getSortedTrips();
		Assert.assertArrayEquals(expectedLegs.toArray(), legs.toArray());
	}

	@Test
	public void test5() {

		Segment seg = new Segment(new Leg(4, 11));
		seg.add(new Leg(4, 6));
		seg.add(new Leg(3, 9));
		seg.add(new Leg(4, 9));
		seg.add(new Leg(3, 5));

		List<Leg> expectedLegs = new ArrayList<Leg>();
		expectedLegs.addAll(Arrays.asList(new Leg(3, 5), new Leg(3, 9), new Leg(4, 6), new Leg(4, 9), new Leg(4, 11)));

		List<Leg> legs = seg.getSortedTrips();
		Assert.assertArrayEquals(expectedLegs.toArray(), legs.toArray());
	}

	@Test
	public void test6() {

		Segment seg = new Segment(new Leg(1, 4));
		seg.add(new Leg(2, 5));
		seg.add(new Leg(6, 10));
		seg.add(new Leg(7, 11));
		seg.add(new Leg(11, 12));
		seg.add(new Leg(1, 12));

		List<Leg> expectedLegs = new ArrayList<Leg>();
		expectedLegs.addAll(Arrays.asList(new Leg(1, 4), new Leg(1, 12), new Leg(2, 5), new Leg(6, 10), new Leg(7, 11),
				new Leg(11, 12)));

		List<Leg> legs = seg.getSortedTrips();
		Assert.assertArrayEquals(expectedLegs.toArray(), legs.toArray());
	}

}
