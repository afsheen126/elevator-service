package com.afsheen.elevator.domain;

import org.junit.Assert;
import org.junit.Test;

public class LegTester {

	@Test
	public void testIfStartIsLesserThanEndDirectionIsUpward() {
		Leg leg = new Leg(5, 9);

		Assert.assertNotNull(leg);
		Assert.assertEquals(5, leg.getStart());
		Assert.assertEquals(9, leg.getEnd());
		Assert.assertTrue(leg.isGoingUp());
		Assert.assertFalse(leg.isGoingDown());
	}

	@Test
	public void testIfStartIsGreaterThanEndDirectionIsDownward() {
		Leg leg = new Leg(9, 5);

		Assert.assertNotNull(leg);
		Assert.assertEquals(9, leg.getStart());
		Assert.assertEquals(5, leg.getEnd());
		Assert.assertFalse(leg.isGoingUp());
		Assert.assertTrue(leg.isGoingDown());
	}

	@Test
	public void testIfTwoLegsAreGoingInSameDirection() {
		Leg leg = new Leg(9, 5);
		Leg other = new Leg(7, 4);

		Assert.assertTrue(leg.isGoingInSameDirection(other));
		Assert.assertFalse(leg.isGoingInOppositeDirection(other));
	}

	@Test
	public void testIfTwoLegsAreGoingInOppositeDirection() {
		Leg leg = new Leg(9, 5);
		Leg other = new Leg(7, 9);

		Assert.assertFalse(leg.isGoingInSameDirection(other));
		Assert.assertTrue(leg.isGoingInOppositeDirection(other));
	}

	@Test
	public void testIfTwoLegsAreEqual() {
		Leg leg1 = new Leg(9, 5);
		Leg leg2 = new Leg(9, 5);
		Leg leg3 = new Leg(9, 6);

		Assert.assertTrue(leg1.equals(leg1));
		Assert.assertTrue(leg1.equals(leg2));
		Assert.assertFalse(leg1.equals(null));
		Assert.assertFalse(leg1.equals(leg3));
		Assert.assertFalse(leg2.equals(leg3));
	}

	@Test
	public void testLegComparisonForDownTrips() {
		Leg leg1 = new Leg(9, 5);
		Leg leg2 = new Leg(8, 7);
		Leg leg3 = new Leg(10, 1);
		Leg leg4 = new Leg(9, 5);

		Assert.assertTrue(leg1.compareTo(leg2) < 0);
		Assert.assertTrue(leg1.compareTo(leg3) > 0);
		Assert.assertTrue(leg1.compareTo(leg4) == 0);
		Assert.assertTrue(leg1.compareTo(leg1) == 0);
		Assert.assertTrue(leg2.compareTo(leg3) > 0);
	}

	@Test
	public void testLegComparisonForUpTrips() {
		Leg leg1 = new Leg(5, 9);
		Leg leg2 = new Leg(7, 8);
		Leg leg3 = new Leg(1, 10);
		Leg leg4 = new Leg(5, 9);

		Assert.assertTrue(leg1.compareTo(leg2) < 0);
		Assert.assertTrue(leg1.compareTo(leg3) > 0);
		Assert.assertTrue(leg1.compareTo(leg4) == 0);
		Assert.assertTrue(leg1.compareTo(leg1) == 0);
		Assert.assertTrue(leg2.compareTo(leg3) > 0);
	}

}
