package com.afsheen.elevator.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.afsheen.elevator.service.ElevatorServiceInvoker;
import com.afsheen.elevator.service.ElevatorTripPlan;

/**
 * The main class of the Elevator application. This will be called from the
 * command-line with two arguments.
 * 
 * <pre>
 * Example usage :
 *    java -cp elevator-service-0.0.1-SNAPSHOT.jar com.afsheen.elevator.main.Elevator a.txt B
 * </pre>
 * 
 * The main() method takes in two arguments :
 * <ul>
 * <li>name of the file with the inputs</li>
 * <li>operating mode as a single character : A or B</li>
 * </ul>
 * 
 * The two arguments are used to create an ElevatorServiceInvoker object.
 * 
 * The result of the elevator trip planning algorithm is returned as a list of
 * ElevatorTripPlan objects. Details from the objects are printed to the console
 * here.
 * 
 * If any exceptions are thrown, they are also written to console by the main
 * method here.
 * 
 * @author Afsheen
 *
 */
public class Elevator {

	public static void main(String[] args) {

		if (args.length < 2) {
			System.err.println(
					"Usage : java -cp elevator-service-0.0.1-SNAPSHOT.jar com.afsheen.elevator.main.Elevator <path-to-file> <mode of operation, enter single character: A or B>");
			System.exit(-1);
		}

		String inputFileName = args[0];
		String modeString = args[1];

		try {
			ElevatorServiceInvoker elevatorServiceInvoker = new ElevatorServiceInvoker(modeString, inputFileName);
			List<ElevatorTripPlan> plans = elevatorServiceInvoker.execute();

			for (ElevatorTripPlan curr : plans) {
				// output of the elevator trip planning algorithm
				// is received as ElevatorTripPlan objects that
				// are printed to the console
				System.out.println(curr.getTripPlan());
			}

		} catch (FileNotFoundException e) {
			System.err.println("ERROR : Unable to open file : " + inputFileName);
		} catch (IOException e) {
			System.err.println("ERROR : Unable to read from file : " + inputFileName);
		} catch (IllegalArgumentException ex) {
			System.err.println("ERROR : " + ex.getMessage());
		} catch (Exception ex) {
			System.err.println("ERROR : " + ex.getMessage());
		}
	}

}
