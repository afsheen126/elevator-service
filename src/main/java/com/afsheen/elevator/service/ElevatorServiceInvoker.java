package com.afsheen.elevator.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.afsheen.elevator.domain.ElevatorOperatingMode;

/**
 * This class takes in the argument from the application's main method and
 * decides which implementation of ElevatorService to invoke. It reads the input
 * file, validates them and converts to a list of integers that can be passed
 * onto the selected ElevatorService implementation.
 * 
 * The following validations are performed:
 * <ul>
 * <li>the modeString argument corresponds to valid operating mode</li>
 * <li>the file corresponding to the first argument can be opened</li>
 * <li>each line in file follows the INPUT_LINE_PATTERN format</li>
 * </ul>
 * 
 * After the lines are read in from the input file successfully, the service
 * class corresponding to the operating mode is invoked to get the elevator trip
 * plans
 * 
 * @author Afsheen
 *
 */
public class ElevatorServiceInvoker {

	private static final Pattern INPUT_LINE_PATTERN = Pattern.compile("(\\d+):(\\d+-\\d+)((,\\d+-\\d+)*)");

	private final ElevatorOperatingMode mode;
	private final String inputFileName;
	private final ElevatorService elevatorService;

	public ElevatorServiceInvoker(final String modeString, final String inputFileName)
			throws FileNotFoundException, IOException {

		if ((!ElevatorOperatingMode.A.name().equals(modeString))
				&& (!ElevatorOperatingMode.B.name().equals(modeString))) {
			String msg = "Elevator operating mode can only be A or B. Specify single character.";
			throw new IllegalArgumentException(msg);
		}

		if ((inputFileName == null) || (inputFileName.trim().length() == 0)) {
			String msg = "Input file name cannot be empty or blank";
			throw new IllegalArgumentException(msg);
		}

		this.mode = ElevatorOperatingMode.valueOf(modeString);

		this.inputFileName = inputFileName;

		this.elevatorService = (ElevatorOperatingMode.A == mode) ? new ElevatorInModeAServiceImpl()
				: new ElevatorInModeBServiceImpl();
	}

	public List<ElevatorTripPlan> execute() throws FileNotFoundException, IOException {

		// if there are any errors in opening or reading the file
		// or if the lines do not match the expected pattern
		// the inputLines collection will be empty
		List<List<Integer>> inputLines = buildInputLines();

		if (inputLines.isEmpty()) {
			String msg = String.format("Unable to read any valid lines from file : %s", this.inputFileName);
			throw new IllegalArgumentException(msg);
		}

		List<ElevatorTripPlan> plans = new ArrayList<>();

		for (List<Integer> currLine : inputLines) {
			plans.add(elevatorService.getElevatorTripPlan(currLine));
		}

		return plans;
	}

	/**
	 * this method reads the raw text lines from the input file and checks if
	 * each line matches the expected pattern. If there are any errors, none of
	 * the lines are processed and empty collection will be returned.
	 * 
	 * @param inputFileName
	 * @return inputLines - list of list of integers that indicate the floors in
	 *         the requests
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private List<List<Integer>> buildInputLines() throws FileNotFoundException, IOException {
		List<String> rawLines = new ArrayList<>();

		// opening file resources within try-block to ensure that they will be
		// released after usage
		try (FileReader f = new FileReader(inputFileName); BufferedReader reader = new BufferedReader(f)) {

			String currRawLine = null;
			Matcher matcher = null;
			boolean valid = false;
			int line = 0;

			while ((currRawLine = reader.readLine()) != null) {
				// check if the line read from file matches expected pattern
				matcher = INPUT_LINE_PATTERN.matcher(currRawLine);

				valid = matcher.matches();

				if (!valid) {
					String msg = String.format(
							"Input line number %d <%s> does not match expected format. Valid line looks like '10:1-8,4-5'",
							line, currRawLine);
					rawLines.clear();
					throw new IllegalArgumentException(msg);
				}

				rawLines.add(currRawLine);

				line++;
			}
		}

		return buildInputLines(rawLines);
	}

	/**
	 * this method will convert the rawLine of the format
	 * 
	 * <pre>
	 * 10:1-8,8-5
	 * 9:7-2,1-8
	 * </pre>
	 * 
	 * to list of integers that indicate the floors like
	 * 
	 * <pre>
	 * 10 1 1 8 8 5
	 * 9 7 7 2 1 8
	 * </pre>
	 * 
	 * @param rawLines
	 * @return inputLines - list of list of integers that indicate the floors in
	 *         the requests
	 */
	private static List<List<Integer>> buildInputLines(List<String> rawLines) {
		List<List<Integer>> inputLines = new ArrayList<>();

		for (String currRawLine : rawLines) {
			List<Integer> currInputLine = new ArrayList<>();

			// inputLine is of format '10:8-1,4-3'
			// split the string to get the numbers without
			// the three delimiters ':' '-' and ','
			String[] splits = currRawLine.split(":|-|,");

			// elevator needs to go from initial floor to fromFloor
			// of the 1st trip. This is the initial trip

			// add integer for startFloor of the initial trip
			Integer trip0StartFloor = Integer.valueOf(splits[0]);
			currInputLine.add(trip0StartFloor);

			// add integer for endFloor of the initial trip
			Integer trip0EndFloor = Integer.valueOf(splits[1]);
			currInputLine.add(trip0EndFloor);

			for (int index = 1; index < splits.length; index++) {
				currInputLine.add(Integer.valueOf(splits[index]));
			}

			inputLines.add(currInputLine);
		}

		return inputLines;
	}

	public ElevatorOperatingMode getMode() {
		return mode;
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public ElevatorService getElevatorService() {
		return elevatorService;
	}
}
