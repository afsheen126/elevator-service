package com.afsheen.elevator.service;

import java.util.List;

import com.afsheen.elevator.domain.ElevatorInModeA;
import com.afsheen.elevator.domain.Leg;

/**
 * Implements the ElevatorService for ElevatorOperatingMode.A
 * 
 * @author Afsheen
 *
 */
public class ElevatorInModeAServiceImpl implements ElevatorService {

	@Override
	/**
	 * Service method passes the inputs to build a ElevatorInModeA object that
	 * holds the algorithm for operating elevator in Mode A.
	 * 
	 * The ElevatorInModeA object is used build the ElevatorTripPlan object that
	 * can be used by clients.
	 */
	public ElevatorTripPlan getElevatorTripPlan(List<Integer> inputs) {

		List<Leg> legs = Leg.buildLegs(inputs);

		ElevatorInModeA elevatorA = new ElevatorInModeA(legs);

		ElevatorTripPlan plan = new ElevatorTripPlan(legs, elevatorA.getTrips(), elevatorA.getFloors());

		return plan;
	}

}
