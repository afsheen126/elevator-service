package com.afsheen.elevator.service;

import java.util.List;

import com.afsheen.elevator.domain.Leg;

/**
 * Represents the final output of the Elevator trip planning algorithm. Holds
 * info about the trips and the floors at which the elevator will stop.
 * 
 * Also provides the getTripPlan method that returns the trip plan as a string
 * that will be printed to the console.
 * 
 * @author Afsheen
 *
 */
public class ElevatorTripPlan {

	private final List<Leg> input;
	private final List<Leg> trips;
	private final List<Integer> floors;

	public ElevatorTripPlan(List<Leg> input, List<Leg> trips, List<Integer> floors) {
		this.input = input;
		this.trips = trips;
		this.floors = floors;
	}

	public List<Leg> getInput() {
		return this.input;
	}

	public List<Leg> getTrips() {
		return trips;
	}

	public List<Integer> getFloors() {
		return floors;
	}

	private Integer retrieveFloor(int index) {
		if (index < floors.size()) {
			return floors.get(index);
		}

		return null;
	}

	/**
	 * 
	 * @return output string that can be printed by the client to the console.
	 *         Holds information about the floors at which the elevator will
	 *         stop to service the requests.
	 */
	public String getTripPlan() {

		StringBuilder sb = new StringBuilder();

		int index = 0;
		Integer currFloor = retrieveFloor(index);
		Integer nextFloor = null;

		while (currFloor != null) {

			sb.append(currFloor);
			sb.append(" ");

			nextFloor = retrieveFloor(++index);

			while (currFloor.equals(nextFloor)) {

				nextFloor = retrieveFloor(++index);
			}

			currFloor = nextFloor;
		}

		sb.append(String.format("(%d)", getTripDistance()));

		return sb.toString();
	}

	/**
	 * 
	 * @return the total distance traveled by elevator to satisfy the incoming
	 *         trip requests
	 */
	public Integer getTripDistance() {
		int distance = 0;

		int index = 0;
		Integer currFloor = retrieveFloor(index);
		Integer nextFloor = null;

		while (currFloor != null) {

			nextFloor = retrieveFloor(++index);

			if (nextFloor != null) {
				distance += Math.abs(currFloor - nextFloor);
			}

			currFloor = nextFloor;
		}

		return distance;
	}

}
