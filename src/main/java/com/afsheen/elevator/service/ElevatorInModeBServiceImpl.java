package com.afsheen.elevator.service;

import java.util.List;

import com.afsheen.elevator.domain.ElevatorInModeB;
import com.afsheen.elevator.domain.Leg;

/**
 * Implements the ElevatorService for ElevatorOperatingMode.B
 * 
 * @author Afsheen
 *
 */

public class ElevatorInModeBServiceImpl implements ElevatorService {

	@Override
	/**
	 * Service method passes the inputs to build a ElevatorInModeB object that
	 * holds the algorithm for operating elevator in Mode B.
	 * 
	 * The ElevatorInModeB object is used build the ElevatorTripPlan object that
	 * can be used by clients.
	 */
	public ElevatorTripPlan getElevatorTripPlan(List<Integer> inputs) {

		List<Leg> legs = Leg.buildLegs(inputs);

		ElevatorInModeB elevatorB = new ElevatorInModeB(legs);

		ElevatorTripPlan plan = new ElevatorTripPlan(legs, elevatorB.getTrips(), elevatorB.getFloors());

		return plan;
	}

}
