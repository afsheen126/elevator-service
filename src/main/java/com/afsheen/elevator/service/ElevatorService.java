package com.afsheen.elevator.service;

import java.util.List;

/**
 * Represents the service interface that is expected to be implemented by the
 * different elevator trip planning algorithms.
 * 
 * @author Afsheen
 *
 */

public interface ElevatorService {

	/**
	 * 
	 * @param inputs
	 *            list of integers that point to the floors in the elevator
	 *            request. For e.g., 4,1,3,2,1,5 will represent the trips 4-1,
	 *            3-2 and 1-5.
	 * 
	 * @return ElevatorTripPlan object that holds info about the trip plan
	 *         built by the implementing algorithm. Fields of ElevatorTrip
	 *         plan object can be queried for more information.
	 */
	public ElevatorTripPlan getElevatorTripPlan(List<Integer> inputs);
}
