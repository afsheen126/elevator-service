package com.afsheen.elevator.domain;

/**
 * Enum to represent the two operating modes available for the elevator
 * 
 * @author Afsheen
 *
 */
public enum ElevatorOperatingMode {

	/** operating mode A */
	A,

	/** operating mode B */
	B
}
