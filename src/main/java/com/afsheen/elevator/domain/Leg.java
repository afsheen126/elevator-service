package com.afsheen.elevator.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents movement of elevator from starting floor to ending floor.
 * 
 * 
 * @author Afsheen
 *
 */
public class Leg implements Comparable<Leg> {

	/**
	 * Enum to indicate if elevator is going down or up
	 * 
	 */
	public static enum LegType {
		DOWNWARD, UPWARD
	};

	private final int start;
	private final int end;
	private final LegType type;

	public Leg(final int start, final int end) {

		this.start = start;
		this.end = end;

		type = (start < end) ? LegType.UPWARD : LegType.DOWNWARD;
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public boolean isGoingDown() {
		return (type == LegType.DOWNWARD);
	}

	public boolean isGoingUp() {
		return (type == LegType.UPWARD);
	}

	public boolean isGoingInSameDirection(Leg other) {
		return (other != null) && (this.type == other.type);
	}

	public boolean isGoingInOppositeDirection(Leg other) {
		return (other != null) && (this.type != other.type);
	}

	@Override
	public String toString() {
		return String.format("%d-%d", start, end);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Leg)) {
			return false;
		}

		Leg other = (Leg) o;

		if (start != other.start) {
			return false;
		} else if (end != other.end) {
			return false;
		}

		return true;
	}

	/**
	 * For upward trips
	 * <ul>
	 * <li>(2,4) is before (3,5)</li>
	 * <li>(2,4) is after (1,3)</li>
	 * <li>(2,4) is before (2,6)</li>
	 * <li>(2,4) is after (2,1)</li>
	 * </ul>
	 * 
	 * 
	 * For downward trips
	 * <ul>
	 * <li>(5,3) is before (4,2)</li>
	 * <li>(3,1) is after (4,2)</li>
	 * <li>(4,1) is before (4,2)</li>
	 * <li>(4,3) is after (4,2)</li>
	 * </ul>
	 */
	@Override
	public int compareTo(Leg o) {

		if ((this.start == o.start) && (this.end == o.end)) {
			return 0;
		}

		int comp = 0;

		if (this.start < o.start) {
			comp = -1;
		} else if (this.start > o.start) {
			comp = 1;
		} else if (this.end < o.end) {
			comp = -1;
		} else if (this.end > o.end) {
			comp = 1;
		}

		if (isGoingUp()) {
			return comp;
		}

		// for downward trips return value should be inverted
		return (-1) * comp;
	}

	/**
	 * Helper method to convert the input requests into the corresponding Leg
	 * objects. The algorithms work on Leg objects, but the clients need to send
	 * in only integers corresponding the floors.
	 * 
	 * @param requests
	 * @return list of corresponding Leg objects that can be passed to the
	 *         algorithm objects
	 */
	public static List<Leg> buildLegs(List<Integer> requests) {
		List<Leg> legs = new ArrayList<>();

		for (int index = 0; index < (requests.size() - 1); index += 2) {

			Integer fromFloor = requests.get(index);
			Integer toFloor = requests.get(index + 1);

			legs.add(new Leg(fromFloor, toFloor));
		}

		return legs;
	}
}
