package com.afsheen.elevator.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Holds the trip planning algorithm for operating the Elevator in mode B.
 * Invoked by ElevatorInModeBServiceImpl
 * 
 * @author Afsheen
 *
 */
public class ElevatorInModeB {

	private final List<Leg> inputs;
	private final List<Leg> trips;
	private final List<Integer> floors;

	public ElevatorInModeB(final List<Leg> input) {
		this.inputs = input;
		trips = new ArrayList<>();
		floors = new ArrayList<>();

		buildTripsList();
		buildFloorsList();
	}

	private void buildTripsList() {
		if (inputs.isEmpty())
			return;

		int index = 0;
		Leg currLeg = retrieveInputLeg(index);

		while (currLeg != null) {
			Segment currSegment = new Segment(currLeg);

			// move index and retrieve next leg from input list
			Leg nextLeg = retrieveInputLeg(++index);

			// look at the next legs in the input list. As long as
			// they are legs that are going in the same direction,
			// they will be added to the current segment.
			while (currLeg.isGoingInSameDirection(nextLeg)) {
				currSegment.add(nextLeg);

				nextLeg = retrieveInputLeg(++index);
			}

			// previous loop would have been broken when you found a leg
			// going in the opposite direction or the end of the input
			// list was reached. The currSegment object will sort the legs that
			// were added to it and return it to be added to the trip plan
			if (currSegment.isNotEmpty()) {
				trips.addAll(currSegment.getSortedTrips());
			}

			currLeg = nextLeg;
		}

	}

	private void buildFloorsList() {
		if (trips.isEmpty())
			return;

		int index = 0;
		Leg currLeg = retrieveTripLeg(index);

		while (currLeg != null) {
			Segment currSegment = new Segment(currLeg);

			// move index and retrieve next leg from trip list
			Leg nextLeg = retrieveTripLeg(++index);

			// look at the next legs in the input list. As long as
			// they are legs that are going in the same direction,
			// they will be added to the current segment.
			while (currLeg.isGoingInSameDirection(nextLeg)) {
				currSegment.add(nextLeg);

				nextLeg = retrieveTripLeg(++index);
			}

			// previous loop would have been broken when you found a leg
			// going in the opposite direction or the end of the input
			// list was reached. The currSegment object will use the floors
			// of the trip legs that were added to it to get the list of
			// floors at which the elevator will stop to execute the trip plan
			if (currSegment.isNotEmpty()) {
				floors.addAll(currSegment.getSortedFloors());
			}

			currLeg = nextLeg;
		}
	}

	private Leg retrieveInputLeg(int index) {
		if (index < inputs.size()) {
			return inputs.get(index);
		}

		return null;
	}

	private Leg retrieveTripLeg(int index) {
		if (index < trips.size()) {
			return trips.get(index);
		}

		return null;
	}

	public List<Leg> getTrips() {
		return this.trips;
	}

	public List<Integer> getFloors() {
		return this.floors;
	}

	/**
	 * Helper class used by ElevatorInModeB to hold and order group of similar
	 * trips. This class holds the invariant that it can only hold trips that
	 * are going in the same direction
	 * 
	 * The direction of the segment is the direction of the first leg added to
	 * it. Subsequent legs can be added only if they are in the same direction
	 * as the first.
	 * 
	 */
	static class Segment {

		private final List<Leg> legs;
		private final Leg firstLeg;

		public Segment(Leg leg) {
			if (leg == null)
				throw new IllegalArgumentException("Unable to build trip segment");

			this.firstLeg = leg;

			legs = new ArrayList<Leg>();
			legs.add(firstLeg);
		}

		public void add(Leg nextLeg) {
			if (nextLeg == null)
				throw new IllegalArgumentException("Unable to build trip segment");

			if (isGoingInOppositeDirection(nextLeg)) {
				throw new IllegalArgumentException("Legs going in opposite direction cannot be added to segment");
			}

			legs.add(nextLeg);
		}

		public List<Leg> getSortedTrips() {
			List<Leg> sortedLegs = new ArrayList<>(legs);

			Collections.sort(sortedLegs);

			return sortedLegs;
		}

		public List<Integer> getSortedFloors() {

			List<Integer> floors = new ArrayList<>();

			for (Leg currLeg : legs) {
				floors.add(Integer.valueOf(currLeg.getStart()));
				floors.add(Integer.valueOf(currLeg.getEnd()));
			}

			if (isGoingUp()) {
				Collections.sort(floors);
			} else {
				Collections.sort(floors, Collections.reverseOrder());
			}

			return floors;
		}

		public boolean isEmpty() {
			return legs.isEmpty();
		}

		public boolean isNotEmpty() {
			return (!isEmpty());
		}

		public boolean isGoingDown() {
			return (firstLeg.isGoingDown());
		}

		public boolean isGoingUp() {
			return (firstLeg.isGoingUp());
		}

		public boolean isGoingInSameDirection(Leg other) {
			return (firstLeg.isGoingInSameDirection(other));
		}

		public boolean isGoingInOppositeDirection(Leg other) {
			return (firstLeg.isGoingInOppositeDirection(other));
		}
	}
}
