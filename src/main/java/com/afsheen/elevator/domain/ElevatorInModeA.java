package com.afsheen.elevator.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds the trip planning algorithm for operating the Elevator in mode A.
 * Invoked by ElevatorInModeAServiceImpl
 * 
 * 
 * @author Afsheen
 *
 */
public class ElevatorInModeA {

	private final List<Leg> inputs;
	private final List<Leg> trips;
	private final List<Integer> floors;

	public ElevatorInModeA(final List<Leg> input) {
		this.inputs = input;
		trips = new ArrayList<>();
		floors = new ArrayList<>();

		buildTripsList();
		buildFloorsList();
	}

	private void buildTripsList() {
		if (inputs.isEmpty())
			return;

		for (Leg currLeg : inputs) {
			trips.add(currLeg);
		}
	}

	private void buildFloorsList() {
		if (trips.isEmpty())
			return;

		for (Leg currLeg : trips) {
			floors.add(currLeg.getStart());
			floors.add(currLeg.getEnd());
		}
	}

	public List<Leg> getTrips() {
		return this.trips;
	}

	public List<Integer> getFloors() {
		return this.floors;
	}

}
