==================================
README for Elevator Application

By Afsheen Rajendran
==================================

INTRODUCTION

- This project was created for solving the Elevator problem.

- This project can be run in Java 7. 

- The compiler settings have been specified in the maven pom file.


RUNNING THE APPLICATION

- The elevator-service.jar file in 'build' directory can be used for running the application from the command-line.

- In the command-line, go to the location of elevator-service directory (where you downloaded it).

- run the command 
        'java -cp build/elevator-service.jar com.afsheen.elevator.main.Elevator inp.txt B'
  where 
     - inp.txt is the file containing the elevator requests 
     - and B is the operating mode requested.

- The input file will have lines in the following format
10:8-1
2:4-1,4-2,6-8
- The output will be the elevator trip plan info that will be printed to the console


USING MAVEN TO CREATE THE JAR 

- The elevator-service project can be compiled and packaged using maven

- The maven pom.xml file is located at elevator-service/pom.xml

- In the command-line, go to the location of elevator-service directory

- type 'mvn clean install -DskipTests=true' to compile and package the application

- The packaged output will be located at target/elevator-service-0.0.1-SNAPSHOT.jar


USING MAVEN TO RUN THE TESTS

- You will need to update the file path location in the ElevatorServiceInvokerTester.java
  before running the tests through maven
  
- Edit the BASE_PATH variable in src/test/resources/com/afsheen/elevator/service/ElevatorServiceInvokerTester.java
  to point to the correct location (based on where you downloaded the project)
  
- type 'mvn clean install' to compile, run tests and package the application   

- The packaged output will be located at target/elevator-service-0.0.1-SNAPSHOT.jar  







